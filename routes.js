const routes = (module.exports = require("next-routes")());

routes
  .add("home", "/", "index")
  .add("trayectos", "/trayectos", "trayectos")
  .add("viajes", "/viajes", "viajes");
