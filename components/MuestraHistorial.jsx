import React from "react";

const MuestraHistorial = ({ data }) => {
  return (
    <table className='table'>
      <thead className='thead-dark'>
        <tr>
          <th scope='col'>id</th>
          <th scope='col'>Fecha</th>
          <th scope='col'>Odometro</th>
          <th scope='col'>Trayecto</th>
        </tr>
      </thead>
      <tbody>
        {data.map((item, index) => (
          <Fila key={item._id} trayecto={item} index={index} />
        ))}
      </tbody>
    </table>
  );
};

export default MuestraHistorial;

const Fila = ({ trayecto, index }) => {
  return (
    <tr>
      <th scope='row'>{index + 1}</th>
      <td>{trayecto.fecha}</td>
      <td>{trayecto.odometro}</td>
      <td>{trayecto.trayecto}</td>
    </tr>
  );
};
