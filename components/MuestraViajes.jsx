import React from "react";

const MuestraViajes = ({ data }) => {
  return (
    <table className='table'>
      <thead className='thead-dark'>
        <tr>
          <th scope='col'>id</th>
          <th scope='col'>Fecha</th>
          <th scope='col'>Km</th>
        </tr>
      </thead>
      <tbody>
        {data.map((item, index) => (
          <Fila key={item._id} viaje={item} index={index} />
        ))}
      </tbody>
    </table>
  );
};

export default MuestraViajes;

const Fila = ({ viaje, index }) => {
  return (
    <tr>
      <th scope='row'>{index + 1}</th>
      <td>{viaje.fecha}</td>
      <td>{viaje.km}</td>
    </tr>
  );
};
