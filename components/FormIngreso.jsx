import React, { Fragment, useState } from "react";
import { SET_TRAYECTOS, SET_VIAJE } from "../querys/qTrayectos";
import Router from "next/router";
import { Mutation } from "react-apollo";

const FormIngreso = ({ odometroIn, trayecto, idInicio }) => {
  const [odometro, setOdometro] = useState("");

  const insertar = async mutate => {
    const response = await mutate({
      variables: {
        input: {
          odometro: parseInt(odometro, 10),
          trayecto: "inicio"
        }
      }
    });
    if (response) {
      Router.reload("/");
    }
  };

  const insertarViaje = async mutate => {
    const response = await mutate({
      variables: {
        idInicio: idInicio,
        input: {
          odometro: parseInt(odometro, 10),
          trayecto: "fin"
        }
      }
    });
    if (response) {
      Router.reload("/");
    }
  };
  const inicial = () => {
    return (
      <Fragment>
        <div className='form-row'>
          <div className='form-group col-md-12'>
            <label htmlFor='odometro'>Odometro</label>
            <input
              placeholder='Ingrese odometro'
              value={odometro}
              onChange={e => setOdometro(e.target.value)}
              className='form-control'
              id='odometro'
            />
          </div>
        </div>
        <Mutation mutation={SET_TRAYECTOS}>
          {mutate => (
            <button
              onClick={() => insertar(mutate)}
              className='btn btn-primary w-100'
            >
              Enviar
            </button>
          )}
        </Mutation>
      </Fragment>
    );
  };

  const final = () => {
    return (
      <div className='form-row'>
        <div className='form-group col-md-12'>
          <label htmlFor='odometro'>Odometro</label>
          <input
            placeholder='Ingrese odometro'
            value={odometro}
            onChange={e => setOdometro(e.target.value)}
            className='form-control'
            id='odometro'
          />
        </div>
        <Mutation mutation={SET_VIAJE}>
          {mutate => (
            <button
              onClick={() => insertarViaje(mutate)}
              className='btn btn-primary w-100'
            >
              Enviar
            </button>
          )}
        </Mutation>
      </div>
    );
  };

  if (trayecto === "fin") {
    return (
      <Fragment>
        <div className='form-row'>
          <div className='form-group col-md-12'>
            <label htmlFor='odometro'>Odometro</label>
            <label className='form-control' id='odometro'>
              {odometroIn}
            </label>
          </div>
        </div>
        <Fragment>
          <h3>Fin</h3>
          <div className='grid'>{final()}</div>
        </Fragment>
      </Fragment>
    );
  }

  return (
    <Fragment>
      <h3>Inicio</h3>
      <div className='grid'>{inicial()}</div>
    </Fragment>
  );
};

export default FormIngreso;
