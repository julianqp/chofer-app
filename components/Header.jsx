import { Link } from "../routes";
import Head from "next/head";

export default class Layout extends React.Component {
  render() {
    const { children, title } = this.props;

    return (
      <div>
        <Head>
          <title>{title}</title>
          <link
            rel='stylesheet'
            href='https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css'
            integrity='sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T'
            crossOrigin='anonymous'
          />
          <meta name='viewport' content='width=device-width' />
        </Head>

        <header>
          <Link route='home'>
            <a>Ingreso</a>
          </Link>
          <Link route='trayectos'>
            <a>Trayectos</a>
          </Link>
          <Link route='viajes'>
            <a>Viajes</a>
          </Link>
        </header>

        {children}

        <style jsx>{`
          header {
            color: #fff;
            background: #455a64;
            width: 100%;
            padding: 15px;
            display: flex;
            text-align: center;
          }
          header a {
            margin-left: 5px;
            margin-right: 5px;
            color: #fff;
            text-decoration: none;
          }
          header a:hover {
            color: #2196f3;
          }
        `}</style>

        <style jsx global>{`
          body {
            margin: 0;
            font-family: system-ui;
            background: white;
          }
        `}</style>
      </div>
    );
  }
}
