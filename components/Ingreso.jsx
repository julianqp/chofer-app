import React, { Fragment, useState } from "react";
import FormIngreso from "./FormIngreso";
import { Query } from "react-apollo";
import { GET_TRAYECTO_INICIAL } from "../querys/qTrayectos";

const Ingreso = ({ data }) => {
  if (data.getTayectoInicio.OK) {
    return (
      <FormIngreso
        odometroIn={data.getTayectoInicio.trayecto.odometro}
        trayecto='fin'
        idInicio={data.getTayectoInicio.trayecto._id}
      />
    );
  } else {
    return <FormIngreso trayecto='inicio' />;
  }
};

export default Ingreso;
