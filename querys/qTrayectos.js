import gql from "graphql-tag";

export const GET_TRAYECTOS = gql`
  {
    getTrayectos {
      _id
      fecha
      odometro
      trayecto
    }
  }
`;

export const GET_VIAJES = gql`
  {
    getViajes {
      _id
      idInicio
      idFin
      km
      fecha
    }
  }
`;

export const GET_TRAYECTO_INICIAL = gql`
  {
    getTayectoInicio {
      OK
      trayecto {
        _id
        odometro
      }
    }
  }
`;

export const SET_TRAYECTOS = gql`
  mutation createTrayecto($input: TrayectoInput) {
    createTrayecto(input: $input) {
      _id
      fecha
      odometro
      trayecto
    }
  }
`;

export const SET_VIAJE = gql`
  mutation createViaje($idInicio: String, $input: TrayectoInput) {
    createViaje(idInicio: $idInicio, input: $input) {
      _id
      idInicio
      idFin
      km
    }
  }
`;
